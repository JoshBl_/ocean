.PHONY: clean
clean:
	find bin -name "ocean*" -exec rm {} \;

build-all: main.go
	GOOS=darwin GOARCH=amd64 go build -v -o bin/darwin-amd64/ocean .
	GOOS=linux GOARCH=amd64 go build -v -o bin/linux-amd64/ocean .
	GOOS=windows GOARCH=amd64 go build -v -o bin/windows-amd64/ocean.exe .
